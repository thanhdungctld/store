import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import Tabletop from 'tabletop';
import { Button, Card, CardBody, CardLink, CardSubtitle, CardText, CardTitle, Collapse, DropdownItem, DropdownMenu, DropdownToggle, Nav, Navbar, NavbarBrand, NavbarText, NavbarToggler, NavItem, NavLink, UncontrolledDropdown } from 'reactstrap';
import MenuNavbar from './menuNavbar';
import SilderBanner from './silder-banner';



export default function Home() {
  const [data, setData] = useState([]);
  useEffect(() => {
    Tabletop.init({
        key: '1PX9UhmoZ3-SqVF32-gdhJEYOd2q9i2_8U82jC-rVxdM',
        callback: googleData => {
            setData(googleData);
            console.log(googleData);
        },
        simpleSheet: true
      })
    }, [])
    return (
        <div >
        <MenuNavbar className="pb-2" />
        <SilderBanner className="pt-2"/>
        <Card>
            <CardBody>
            <CardTitle tag="h5">Card title</CardTitle>
            <CardSubtitle tag="h6" className="mb-2 text-muted">Card subtitle</CardSubtitle>
            </CardBody>
            <img width="100%" src="" alt="Card image cap" />
            <CardBody>
            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
            <CardLink href="#">Card Link</CardLink>
            <CardLink href="#">Another Link</CardLink>
            </CardBody>
        </Card>
        </div>
    
  )
}
