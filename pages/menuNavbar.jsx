import { useState } from 'react';
import { Button, Card, CardBody, CardLink, CardSubtitle, CardText, CardTitle, Collapse, DropdownItem, DropdownMenu, DropdownToggle, Nav, Navbar, NavbarBrand, NavbarText, NavbarToggler, NavItem, NavLink, UncontrolledDropdown } from 'reactstrap';

export default function MenuNavbar() {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar color="light" light expand="md">
      <NavbarBrand href="/">reactstrap</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
              <NavItem>
              <NavLink href="/components/">Components</NavLink>
              </NavItem>
              <NavItem>
              <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                  Options
              </DropdownToggle>
              <DropdownMenu right>
                  <DropdownItem>
                  Option 1
                  </DropdownItem>
                  <DropdownItem>
                  Option 2
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                  Reset
                  </DropdownItem>
              </DropdownMenu>
              </UncontrolledDropdown>
          </Nav>
        <NavbarText>Simple Text</NavbarText>
      </Collapse>
    </Navbar>
    
  )
}
